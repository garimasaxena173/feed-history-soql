import { LightningElement, track } from 'lwc';

import fetchFieldHistory from "@salesforce/apex/HistoryController.fetchFieldHistory";

export default class FetchFieldHistory extends LightningElement {

    @track showComp;
    @track dataObj;
    @track showSpinner;
    @track accountName = 'Test';

    connectedCallback(){
        this.showSpinner = true;
        fetchFieldHistory({
            accountName: this.accountName
            })
            .then(result => {
                if (result && result.length > 0) {
                    this.dataObj = result;
                    this.showComp = true;
                }else{
                    this.showComp = false;
                }
            })
            .catch(error => {
                console.log('error ' + error);
            })
            .finally(() => this.showSpinner = false);
    }
}