public with sharing class HistoryController {

    @AuraEnabled
    public static List<fieldHistoryWrapper> fetchFieldHistory(String accountName) {
        try {
            List<AccountHistory> historyRecords = getFieldHistoryFromBizInfo(accountName);
            List<fieldHistoryWrapper> resultList = new List<fieldHistoryWrapper>();
            for(AccountHistory temp : historyRecords){
                fieldHistoryWrapper wrapper = new fieldHistoryWrapper();
                wrapper.oldValue = (String)temp.OldValue;
                wrapper.newValue = (String)temp.NewValue;
                DateTime dT = temp.CreatedDate;
                Date myDate = date.newinstance(dT.year(), dT.month(), dT.day());
                wrapper.createdDate = myDate;
                resultList.add(wrapper);
            }
            return resultList;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public static List<AccountHistory> getFieldHistoryFromBizInfo(String accountName){
        return [SELECT CreatedDate, OldValue, NewValue, Field 
                FROM AccountHistory 
                WHERE Field = 'Type' 
                AND Name =:accountName
                WITH SECURITY_ENFORCED]; 
    }

    public class fieldHistoryWrapper{
        @AuraEnabled public String oldValue;
        @AuraEnabled public String newValue;
        @AuraEnabled public Date createdDate;
    }
}
